package edu.virginia.hotcoldtours;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class ListScreen extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listscreen);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list_screen, menu);
		return true;
	}

}
